<?php

class Validate
{
    public function scheduleCreate(array $data)
    {
        if (!$data['name'] || !$data['email'] || !$data['task']) {
            return false;
        }

        $name = (string)trim(htmlspecialchars($data['name']));
        $email = (string)filter_var($data['email'], FILTER_VALIDATE_EMAIL);
        $task = (string)trim(htmlspecialchars($data['task']));

        if ($email === false) {
            return false;
        }

        return ['name' => $name, 'email' => $email, 'task' => $task];
    }

    public function auth(array $data)
    {
        if (!$data['login'] || !$data['password']) {
            return false;
        }

        $login = (string)trim(htmlspecialchars($data['login']));
        $password = (string)trim(htmlspecialchars($data['password']));

        return ['login' => $login, 'password' => $password];
    }

    public function updateTask(array $data)
    {
        if (!$data['id'] || !$data['name'] || !$data['email'] || !$data['task']) {
            return false;
        }

        $name = (string)trim(htmlspecialchars($data['name']));
        $email = (string)filter_var($data['email'], FILTER_VALIDATE_EMAIL);
        $task = (string)trim(htmlspecialchars($data['task']));
        $status = !empty($data['status']) ? (bool)$data['status'] : 0;
        $id = (int)trim(htmlspecialchars($data['id']));


        if ($email === false) {
            return false;
        }

        return ['name' => $name, 'email' => $email, 'task' => $task, 'status' => $status, 'id' => $id];
    }
}
