<?php

class AdminModel extends Model
{
    public function auth($login, $password)
    {
        try {
            $sth = $this->dbh->prepare('SELECT * from users WHERE (login=:login) AND (password=:password)');
            $sth->execute(['login' => $login, 'password' => $password]);

            return $sth->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            Route::ErrorPage404();
        }
    }
}
