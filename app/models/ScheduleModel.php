<?php


class ScheduleModel extends Model
{
    public function get()
    {
        try {
            $sth = $this->dbh->prepare('SELECT * from tasks');
            $sth->execute();

            return $sth->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            Route::ErrorPage404();
        }
    }

    public function paginate($page)
    {
        try {
            $sth = $this->dbh->query('SELECT * from tasks LIMIT 3 OFFSET ' . (((int)$page - 1) * 3));
            $sth->execute();

            return $sth->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            Route::ErrorPage404();
        }
    }

    public function find($id)
    {
        try {
            $sth = $this->dbh->prepare('SELECT * from tasks WHERE (id=:id)');
            $sth->execute(['id' => $id]);

            return $sth->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            Route::ErrorPage404();
        }
    }

    public function create_record($data)
    {
        try {
            $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "INSERT INTO tasks (first_name, email, task, status) VALUES ('" . $data['name'] . "', '" . $data['email'] . "', '" . $data['task'] . "',false)";
            $this->dbh->exec($sql);
        } catch (PDOException $e) {
            Route::ErrorPage404();
        }
    }

    public function update($data)
    {
        try {
            $sth = $this->dbh->prepare("UPDATE tasks SET first_name=:first_name, email=:email, task=:task, status=:status, edited=:edited WHERE id=:id");

            $sth->execute([
                'first_name' => $data['name'],
                'email'      => $data['email'],
                'task'       => $data['task'],
                'status'     => $data['status'],
                'id'         => $data['id'],
                'edited'     => true
            ]);

            return $sth->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            Route::ErrorPage404();
        }
    }

    public function remove($id)
    {
        try {
            $sth = $this->dbh->prepare("DELETE FROM tasks WHERE id=:id");

            $sth->execute(['id' => $id]);

            return $sth->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            Route::ErrorPage404();
        }
    }
}