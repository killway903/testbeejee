<?php
require_once __DIR__ . '/../models/ScheduleModel.php';

class Schedule extends Controller
{
    function __construct()
    {
        parent::__construct();

        $this->model = new ScheduleModel();
    }

    public function index($page = 1)
    {
        $numOfPages = abs(count($this->model->get()) / 3);

        if ($page == 0)
            $page = 1;

        if ($page > $numOfPages)
            $page = $numOfPages + 1;


        $tasks = $this->model->paginate($page);

        $this->view->generate('tasks.php', '/views/template.php', [
            'tasks'           => $tasks,
            'current_page'    => $page,
            'number_of_pages' => $numOfPages
        ]);
    }

    public function remove($id)
    {
        if (isset($_SESSION['id']) && !empty($_SESSION['id'])) {
            $this->model->remove($id);

            header('Location: ' . $_SERVER['HTTP_REFERER']);
            exit();
        }

        Route::ErrorPage404();
    }

    public function update()
    {
        if (isset($_SESSION['id']) && !empty($_SESSION['id'])) {
            $validation = (new Validate())->updateTask($_POST);
            $this->model->update($validation);

            header('Location: ' . $_SERVER['HTTP_ORIGIN']);
            exit();
        }

        Route::ErrorPage404();
    }

    public function edit($id = false)
    {
        if (isset($_SESSION['id']) && !empty($_SESSION['id']) && $id) {
            $task = $this->model->find($id);

            $this->view->generate('edit_task.php', '/views/template.php', [
                'task' => $task[0]
            ]);

        } else {
            header('Location: ' . $_SERVER['HTTP_REFERER']);
            exit();
        }
    }

    public function create()
    {
        $validate = Validate::scheduleCreate($_POST);

        if ($validate) {
            $this->model->create_record($validate);

            $_SESSION['created_success'] = true;
            header('Location: ' . $_SERVER['HTTP_REFERER']);
            exit();
        } else {
            Route::ErrorPage404();
        }
    }
}
