<?php

require_once __DIR__.'/../models/AdminModel.php';

class Admin extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->model = new AdminModel();
    }

    public function auth()
    {
        $validation = Validate::auth($_POST);

        if ($validation) {
            $encrypt_password = md5($validation['password']);
            $authData = $this->model->auth($validation['login'], $encrypt_password);
            if (isset($authData[0]['id']) && !empty($authData[0]['id'])) {
                $_SESSION["id"] = $authData[0]['id'];
                $_SESSION["login"] = $authData[0]['login'];
                $_SESSION["first_name"] = $authData[0]['first_name'];
            }
        }

        header('Location: ' . $_SERVER['HTTP_REFERER']);
        exit;
    }

    public function logout()
    {
        session_unset();

        header('Location: /');
        exit;
    }
}
