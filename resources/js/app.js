$('.edit-task').on('click', function (event) {
    let id = this.value;
    window.location.href = '/schedule/edit/' + id;
});

$('.remove-task').on('click', function (event) {
    let id = this.value;
    window.location.href = '/schedule/remove/' + id;
});

$('#logout').on('click', function (event) {
    window.location.href = '/admin/logout';
});


$(document).ready(function () {
    $('.table').DataTable();

    setTimeout(function () {
        $('.alert-success').remove();
    },3000)
});