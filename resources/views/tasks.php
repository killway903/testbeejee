<div class="row d-flex justify-content-between button-bar">
    <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#create-task">Create Task
    </button>
    <?php
    $authenticated = isset($_SESSION['id']) && !empty($_SESSION['id']);
    if ($authenticated) {
        echo 'Hello : ' . $_SESSION['first_name'];
        echo '<button type="button" id="logout" class="btn btn-primary btn-sm">Logout</button>';
    } else {
        echo '
            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#admin-login">Login</button>
        ';
    } ?>
</div>

<div class="align-middle">
    <?php
    if ($_SESSION['created_success']) {
        echo '<div class="alert alert-success" role="alert">Task was successfully created</div>';
        $_SESSION['created_success'] = false;
    }
    ?>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Task</th>
            <th scope="col">Status</th>
            <th scope="col">Edited</th>
            <?php echo $authenticated ? '<th scope="col">Buttons</th>' : '' ?>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($data['tasks'] as $task) {
            $table = '
        <tr>
            <th scope="row">' . $task['id'] . '</th>
            <td>' . $task['first_name'] . '</td>
            <td>' . $task['email'] . '</td>
            <td>' . $task['task'] . '</td>';
            $checked = $task['status'] ? 'checked' : '';
            $status = $task['status'] ? 1 : 0;
            $table .= '<td>
                    <span style="display: none">' . $status . '</span>
                    <div class="form-check">
                    <input disabled ' . $checked . ' class="form-check-input position-static" type="checkbox" 
                    id="blankCheckbox" value="false" aria-label="..." >
                </div></td>';
            $checked = $task['edited'] ? 'checked' : '';

            $table .= '<td><div class="form-check">
                    <input disabled ' . $checked . ' class="form-check-input position-static" type="checkbox" id="blankCheckbox" value="false" aria-label="...">
                </div></td>';


            if ($authenticated) {
                $table .= '<td>
        <button type="button" class="btn btn-primary btn-sm edit-task" value="' . $task["id"] . '">
          Edit
        </button>
        <button type="button" class="btn btn-danger btn-sm remove-task" value="' . $task["id"] . '">
          Remove
        </button>
        </td>';
            }

            echo $table . '</tr>';
        } ?>

        </tbody>
    </table>

    <nav id="pagination" aria-label="...">
        <ul class="pagination pagination-lg">
            <?php

            foreach (range(1, $data['number_of_pages'] + 1) as $page) {
                $is_active = '';

                if ($page == $data['current_page']) {
                    $is_active = 'disabled';
                }

                echo '<li class="page-item ' . $is_active . '">
            <a class="page-link" href="/schedule/index/' . $page . '" tabindex="' . $page . '">' . $page . '</a>
        </li>';
            }
            ?>
        </ul>
    </nav>
    <?php include 'create_task_modal.php'; ?>
    <?php include 'admin_login_modal.php'; ?>

</div>

