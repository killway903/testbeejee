<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Schedule</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include 'styles.html' ?>
</head>
<body>
<div class="container content-wrapper">
    <?php include $content_view; ?>
</div>

<?php include 'scripts.html' ?>

</body>
</html>