<form method="post" action="/schedule/update">
    <div class="form-group">
        <label for="name-input">Name</label>
        <?php echo '  <input name="name" class="form-control" value="' . $data['task']['first_name'] . '">' ?>
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <?php echo '  <input type="email" class="form-control" name="email" aria-describedby="emailHelp"
              value="' . $data['task']['email'] . '">' ?>
    </div>
    <div class="form-group">
        <label for="task-description">Task description</label>
        <?php echo '  <input name="task" class="form-control" value="' . $data['task']['task'] . '">' ?>
    </div>
    <?php echo '<input name="id" type="hidden" value="' . $data['task']['id'] . '">' ?>
    <div class="form-group">
        <label for="status">Status</label>
        <br>

        <?php
        $checked = $data['task']['status'] ? ' checked ' : '';
        echo ' <input name="status" type="checkbox"  '.$checked.' data-toggle="toggle">'
        ?>
    </div>
    <button type="submit" class="btn btn-primary">Update</button>
</form>