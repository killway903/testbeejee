<div id="admin-login" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Create Task</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="post" action="/admin/auth">
                    <div class="form-group">
                        <label for="task-description">Login</label>
                        <input name="login" class="form-control" placeholder="sample" required>
                    </div>

                    <div class="form-group">
                        <label for="task-description">Password</label>
                        <input name="password" type="password" class="form-control" placeholder="********" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>