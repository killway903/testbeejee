<div id="create-task" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Create Task</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form method="post" action="/schedule/create">
                    <div class="form-group">
                        <label for="name-input">Name</label>
                        <input name="name" class="form-control" placeholder="Vasia" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" name="email" aria-describedby="emailHelp" required
                               placeholder="sample@gmail.com">
                    </div>
                    <div class="form-group">
                        <label for="task-description">Task description</label>
                        <input name="task" class="form-control" placeholder="make coffee" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>