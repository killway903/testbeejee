<?php
require_once 'app/Model.php';
require_once 'app/View.php';
require_once 'app/Controller.php';
require_once 'app/validation/Validate.php';
require_once 'app/Route.php';

Route::start();
